import java.util.Random;

/**
 * Created by Loki_ on 26-Oct-16.
 */
public class forCalculate {
    public static void main(String[] args) {

        int[] arrInt = new int[10];
        Random random = new Random();
        for (int i = 0; i < arrInt.length; i++) {
            arrInt[i] = random.nextInt(10) - 7;
        }
        double[] arrDou = new double[10];
        for (int i = 0; i < arrDou.length; i++) {
            arrDou[i] = random.nextDouble() + 5.5;
        }

        System.out.println("Sum int is: " + summaInt(arrInt));
        System.out.println("Sum double is: " + summaDou(arrDou));
        System.out.println("Min int is: " + minnInt(arrInt));
        System.out.println("Min double is: " + minnDou(arrDou));
        System.out.println("Max int is: " + maxxInt(arrInt));
        System.out.println("Max double is: " + maxxDou(arrDou));
        System.out.println("Max positive int is: " + maxPositive(arrInt));
        System.out.println("Max positive double is: " + maxDouPositive(arrDou));
        System.out.println("Multiplication int is: " + multiplicationInt(arrInt));
        System.out.println("Multiplication double is: " + multiplicationDou(arrDou));
        System.out.println("Modulus int is: " + modulusInt(arrInt));
        System.out.println("Modulus double is: " + modulusDou(arrDou));
        System.out.println("Second largest int is: " + secondLargestInt(arrInt));
        System.out.println("Second largest double is: " + secondLargestDou(arrDou));

    }
    private static int summaInt(int[] arrInt){
        int sumInt = 0;
        for (int i = 0; i < arrInt.length; i++) {
            sumInt += arrInt[i];
        }
        return sumInt;
    }

    private static double summaDou(double[] arrDou) {
        double sumDou = 0;
        for (int i = 0; i < arrDou.length; i++) {
            sumDou += arrDou[i];
        }
        return sumDou;
    }

    private static int minnInt(int[] arrInt) {
        int minInt = arrInt[0];
        for (int i = 0; i < arrInt.length; i++) {
            if (arrInt[i] < minInt) {
                minInt = arrInt[i];
            }
        }
        return minInt;
    }

    private static double minnDou(double[] arrDou) {
        double minDou = arrDou[0];
        for (int i = 0; i < arrDou.length; i++) {
            if (arrDou[i] < minDou) {
                minDou = arrDou[i];
            }
        }
        return minDou;
    }

    private static int maxxInt(int[] arrInt) {
        int maxInt = arrInt[0];
        for (int i = 0; i < arrInt.length; i++) {
            if (arrInt[i] > maxInt) {
                maxInt = arrInt[i];
            }
        }
        return maxInt;
    }

    private static double maxxDou(double[] arrDou) {
        double maxDou = arrDou[0];
        for (int i = 0; i < arrDou.length; i++) {
            if (arrDou[i] > maxDou) {
                maxDou = arrDou[i];
            }
        }
        return maxDou;
    }

    private static int maxPositive(int[] arrInt) {
        int maxPosInt = arrInt[0];
        for (int i = 0; i < arrInt.length; i++) {
            if (arrInt[i] > maxPosInt && arrInt[i] > 0) {
                maxPosInt = arrInt[i];
            }
        }
        return maxPosInt;
    }

    private static double maxDouPositive(double[] arrDou) {
        double maxPosDou = arrDou[0];
        for (int i = 0; i < arrDou.length; i++) {
            if (arrDou[i] > maxPosDou && arrDou[i] > 0) {
                maxPosDou = arrDou[i];
            }
        }
        return maxPosDou;
    }

    private static int multiplicationInt(int[] arrInt) {
        int mulInt = 1;
        for (int i = 0; i < arrInt.length; i++) {
            if (arrInt[i] != 0)
            mulInt *= arrInt[i];
        }
        return mulInt;
    }

    private static double multiplicationDou(double[] arrDou) {
        double mulDou = 1;
        for (int i = 0; i < arrDou.length; i++) {
            if (arrDou[i] != 0)
            mulDou *= arrDou[i];
        }
        return mulDou;
    }

    private static int modulusInt(int[] arrInt) {
        int modInt = 0;
        if (arrInt.length != 0) {
            modInt = arrInt[0] % arrInt.length - 1;
        }
        return modInt;
    }

    private static double modulusDou(double[] arrDou) {
        double modDou = 0;
        if (arrDou.length != 0) {
            modDou = arrDou[0] % arrDou.length - 1;
        }
        return modDou;
    }

    private static int secondLargestInt(int[] arrInt) {
        int maxInt = arrInt[0];
        for (int i = 0; i < arrInt.length; i++) {
            if (arrInt[i] > maxInt) {
                maxInt = arrInt[i];
            }
        }
        int secondInt = 0;
        for (int i = 0; i < arrInt.length; i++) {
            if (arrInt[i] > secondInt && arrInt[i] != maxInt) {
                secondInt = arrInt[i];
            }
        }
        return secondInt;
    }

    private static double secondLargestDou(double[] arrDou) {
        double maxDou = arrDou[0];
        for (int i = 0; i < arrDou.length; i++) {
            if (arrDou[i] > maxDou) {
                maxDou = arrDou[i];
            }
        }
        double secondDou = 0.0;
        for (int i = 0; i < arrDou.length; i++) {
            if (arrDou[i] > secondDou && arrDou[i] != maxDou) {
                secondDou = arrDou[i];
            }
        }
        return secondDou;
    }
}
